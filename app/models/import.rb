class Import < ApplicationRecord
  has_attached_file :file
  validates_attachment :file,
                       content_type: {
                         content_type: [
                           'text/plain',
                           'text/csv',
                           'application/vnd.ms-excel',
                           'application/octet-stream'
                         ]
                       },
                       message: "is not in CSV format"

  validates :file, presence: true

  def read_file
    Paperclip.io_adapters.for(file).read
  end
end
