# frozen_literals_true

class SkusController < ApplicationController

  def index
    @skus = Sku.paginate(page: params[:page])
  end

  def create
    import = Import.new(file_params)
    if import.save
      ImportWorker.perform_async('Sku', import.id)
      redirect_to skus_path
    else
      redirect_to root_path, notice: 'Неверный формат файла'
    end
  end
end
