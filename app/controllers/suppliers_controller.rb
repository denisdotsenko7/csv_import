# frozen_literals_true

class SuppliersController < ApplicationController

  def index
    @suppliers = Supplier.paginate(page: params[:page])
  end

  def create
    import = Import.new(file_params)
    if import.save
      ImportWorker.perform_async('Supplier', import.id)
      redirect_to suppliers_path
    else
      redirect_to root_path, notice: 'Неверный формат файла'
    end
  end

  private

  def file_params
    params.permit(:file)
  end
end
