# frozen_string_literal: true

require 'rails_helper'

describe Supplier, type: :model do
  describe 'relations' do
    it { is_expected.to have_many(:skus).with_foreign_key(:supplier_code).with_primary_key(:supplier_code) }
  end

  describe 'validations' do
    it {is_expected.to validate_presence_of(:supplier_code)}
    it {is_expected.to validate_presence_of(:name)}
  end

  describe '#import' do

    context 'with correct file' do
      subject{ Supplier.import(file) }

      let(:file) { fixture_file_upload('suppliers.csv') }

      it { expect{ subject }.to change{ Supplier.count }.by(2) }
    end

    context 'with incorrect file' do
      subject{ Supplier.import(file) }

      let(:file) { fixture_file_upload('wrong_suppliers.csv') }

      it { expect{ subject }.to raise_error(ActiveRecord::RecordInvalid) }
      it { expect{ subject rescue nil }.not_to change{ Supplier.count } }
    end

  end
end
