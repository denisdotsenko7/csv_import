class CreateSuppliers < ActiveRecord::Migration[5.2]
  def change
    create_table :suppliers do |t|
      t.string :supplier_code, null: false
      t.string :name, null: false

      t.timestamps
    end

    add_index :suppliers, :supplier_code, unique: true
  end
end
